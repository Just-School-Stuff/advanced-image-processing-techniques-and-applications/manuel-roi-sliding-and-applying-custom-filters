##################
### REFERENCES ###
##################
# https://pyimagesearch.com/2021/01/19/opencv-bitwise-and-or-xor-and-not/
# https://docs.opencv.org/4.x/d7/d4d/tutorial_py_thresholding.html
# https://stackoverflow.com/questions/64196498/how-to-exit-python-matplotlib-and-continue-code-after-that
# https://stackoverflow.com/questions/42024817/plotting-a-continuous-stream-of-data-with-matplotlib
# https://stackoverflow.com/questions/10580676/comparing-two-numpy-arrays-for-equality-element-wise
# https://numpy.org/doc/stable/reference/random/generated/numpy.random.rand.html
# https://docs.opencv.org/4.x/d7/d4d/tutorial_py_thresholding.html

#################
### LIBRARIES ###
#################
from sys import argv
import matplotlib.pyplot as plt
import numpy as np
import cv2
import concurrent.futures
import threading
import time
# from cv2 import imread, cvtColor, COLOR_RGB2GRAY, threshold, THRESH_BINARY, COLOR_BGR2RGB


###############
### GLOBALS ###
###############
__GLOBAL_LOCKER = threading.Lock()
__GLOBAL_BUFFER = dict()
__IS_PROCESS_DONE = False

#################
### FUNCTIONS ###
#################
def manuel_erosion(image, kernel):
    global __IS_PROCESS_DONE
    function_name = "manuel_erosion"
    
    w_tolerance_1 = int(kernel.shape[0] / 2)
    # w_tolerance_2 = kernel.shape[0] - w_tolerance_1
    
    h_tolerance_1 = int(kernel.shape[1] / 2)
    # h_tolerance_2 = kernel.shape[1] - h_tolerance_1
    
    with __GLOBAL_LOCKER:
        __GLOBAL_BUFFER[function_name] = np.zeros(image.shape)
        
    # __debug_plot_start(function_name)

    # for w in range(0, image.shape[0], kernel.shape[0]):
    #     for h in range(0, image.shape[1], kernel.shape[1]):

    # for w in range(0, image.shape[0], int(kernel.shape[0] / 2)):
    #     for h in range(0, image.shape[1], int(kernel.shape[1] / 2)):
    for w in range(0, image.shape[0], 1):
        for h in range(0, image.shape[1], 1):
            print(f"[INF] Steps | W:{w} - H:{h}", end="\r")
                
            if w - kernel.shape[0] <= 0:
                w_start = 0
                w_end = kernel.shape[0]
            elif w + 1 >= image.shape[0]:
                w_end = image.shape[0] - 1
                w_start = w_end - kernel.shape[0]
            else:
                w_start = w - kernel.shape[0] - w_tolerance_1
                w_end = w_start + kernel.shape[0]

            if h - kernel.shape[1] <= 0:
                h_start = 0
                h_end = kernel.shape[1]
            elif h + 1 >= image.shape[1]:
                h_end = image.shape[1] - 1
                h_start = h_end - kernel.shape[1]
            else:
                h_start = h - h_tolerance_1
                h_end = h_start + kernel.shape[1]

            if w_start < 0 or w_end < 0 or h_start < 0 or h_end < 0 or w_start - w_end > kernel.shape[0] or h_start - h_end > kernel.shape[1]:
                print("[ERR] Calculation Logic Issue")
                
            temp_roi = image[w_start: w_end, h_start: h_end].copy()
            # temp_roi = cv2.bitwise_and(temp_roi, kernel)
            
            # if temp_roi[w_tolerance_1][h_tolerance_1] == kernel[w_tolerance_1][h_tolerance_1] or True:
            if np.array_equal(temp_roi, kernel):
                # temp_roi = np.array(
                #     [
                #         [0, 0, 0, 0, 0],
                #         [0, 1, 0, 0, 0],
                #         [0, 0, 1, 0, 0],
                #         [0, 0, 0, 1, 0],
                #         [0, 0, 0, 0, 0]
                #     ],
                #     np.uint8
                #     # dtype=object
                # )

                temp_roi = np.random.randint(2, size=kernel.shape)
            else:
                temp_roi = kernel.copy()

            
            # time.sleep(0.01)
            with __GLOBAL_LOCKER:
                __GLOBAL_BUFFER[function_name][w_start: w_end,
                                               h_start: h_end] = temp_roi
    __IS_PROCESS_DONE = True
    
    
def window_slider(image, kernel, process_list):
    global __IS_PROCESS_DONE
    
    w_tolerance_1 = int(kernel.shape[0] / 2)
    # w_tolerance_2 = kernel.shape[0] - w_tolerance_1
    
    h_tolerance_1 = int(kernel.shape[1] / 2)
    # h_tolerance_2 = kernel.shape[1] - h_tolerance_1
    
    with __GLOBAL_LOCKER:
        for process in process_list:
            __GLOBAL_BUFFER[str(process)] = np.zeros(image.shape)
        
    for w in range(0, image.shape[0], 1):
        for h in range(0, image.shape[1], 1):
            print(f"[INF] Steps | W:{w} - H:{h}", end="\r")
                
            if w - kernel.shape[0] <= 0:
                w_start = 0
                w_end = kernel.shape[0]
            elif w + 1 >= image.shape[0]:
                w_end = image.shape[0] - 1
                w_start = w_end - kernel.shape[0]
            else:
                w_start = w - kernel.shape[0] - w_tolerance_1
                w_end = w_start + kernel.shape[0]

            if h - kernel.shape[1] <= 0:
                h_start = 0
                h_end = kernel.shape[1]
            elif h + 1 >= image.shape[1]:
                h_end = image.shape[1] - 1
                h_start = h_end - kernel.shape[1]
            else:
                h_start = h - h_tolerance_1
                h_end = h_start + kernel.shape[1]

            if w_start < 0 or w_end < 0 or h_start < 0 or h_end < 0 or w_start - w_end > kernel.shape[0] or h_start - h_end > kernel.shape[1]:
                print("[ERR] Calculation Logic Issue")
                
            temp_roi = image[w_start: w_end, h_start: h_end].copy()
            
            for process in process_list:
                if np.array_equal(temp_roi, kernel):
                    with __GLOBAL_LOCKER:
                        __GLOBAL_BUFFER[str(process)][w_start: w_end,
                                                    h_start: h_end] = process(np.zeros(image.shape))
                else:
                    with __GLOBAL_LOCKER:
                        __GLOBAL_BUFFER[str(process)][w_start: w_end,
                                                    h_start: h_end] = kernel.copy()
    __IS_PROCESS_DONE = True


def erosion(image, kernel, iteration):
    temp = cv2.erode(
        image, kernel, iterations=iteration
    )
    with __GLOBAL_LOCKER:
        __GLOBAL_BUFFER["erosion"] = temp

def corelation(image, kernel):
    return 
    
def main_loop(image, kernel):
    global __IS_PROCESS_DONE
    
    image_threshold = cv2.adaptiveThreshold(
        image.copy(), 1, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
        cv2.THRESH_BINARY_INV, 11, 2
    )
    
    #with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.submit(manuel_erosion, image_threshold, kernel)
        print("[INF] manuel_erosion Thread Started.")
        
        # time.sleep(1)
        # __debug_plot_start("manuel_erosion")

        executor.submit(erosion, image_threshold, kernel, 20)
        print("[INF] erosion Thread Started.")
        
        window_slider(image, kernel, [corelation])
        
        # for i in range(15):
        #     executor.submit(erosion, __GLOBAL_BUFFER["erosion"], kernel)
        #     print(f"[INF] erosion {i} Thread Started.")
        
    # __debug_plot("manuel_erosion")

    while not __IS_PROCESS_DONE:
        time.sleep(0.01)


def show_image(image_list, open_order, title, cmap="gray"):
    length_of_image_list = len(image_list)
    # https://stackoverflow.com/questions/46615554/how-to-display-multiple-images-in-one-figure-correctly/46616645

    # figure = plt.figure(figsize=figsize)
    figure = plt.figure()

    columns = open_order
    if open_order >= length_of_image_list:
        rows = 1
    else:
        if (length_of_image_list % open_order) > 0:
            rows = int(length_of_image_list / open_order) + 1
        else:
            rows = int(length_of_image_list / open_order)

    for i in range(1, length_of_image_list + 1):
        figure.add_subplot(rows, columns, i)

        if i <= len(title):
            plt.title("{}".format(title[i - 1]))

        if len(image_list[i - 1].shape) == 2:
            plt.imshow(image_list[i - 1], cmap=cmap)
        else:
            plt.imshow(cv2.cvtColor(
                image_list[i - 1], cv2.COLOR_BGR2RGB))
    
    plt.show()


def get_Image(path=".", index=0):
    from os import walk
    filenames = [
        file for file in next(walk(path), (None, None, []))[2] if
        ".png" in file or
        ".jpeg" in file or
        ".jpg" in file
    ]
    print(filenames)
    return filenames[index] if len(filenames) > index else ""


#############
### DEBUG ###
#############


def __debug_plot_start(function_name):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.submit(__debug_plot, function_name)


def __debug_plot(function_name):
    keep_ploting = True

    def on_key(event):
        nonlocal keep_ploting
        keep_ploting = False

    plt.ion()
    figure = plt.figure()
    # ax1 = figure.add_subplot(111)

    if function_name in __GLOBAL_BUFFER:
        while keep_ploting:
            plt.imshow(__GLOBAL_BUFFER[function_name], cmap="gray")
            plt.draw()
            plt.pause(0.001)
            figure.canvas.mpl_connect('key_press_event', on_key)
        # plt.show(block=False)
    else:
        print(f"function_name '{function_name}' is not in __GLOBAL_BUFFER")



if __name__ == '__main__':
    image = cv2.imread(argv[1], 0) if len(argv) > 1 else \
            cv2.imread(get_Image(), 0)
    kernel = np.array(
        [
            [0, 0, 0],
            [0, 1, 1],
            [0, 1, 1]
        ],
        np.uint8
    )
    # kernel=np.array(
    #     [
    #         [0, 0, 0, 0, 0],
    #         [0, 1, 0, 0, 0],
    #         [1, 1, 1, 0, 1],
    #         [0, 1, 0, 1, 0],
    #         [0, 0, 0, 1, 0],
    #     ],
    #     np.uint8
    # )
    # kernel = np.ones((5, 5), np.uint8)

    print(f"Image Shape: {image.shape} | Kernel Shape: {kernel.shape}")
    main_loop(image, kernel)

    title_list = [title for title in __GLOBAL_BUFFER.keys()]
    title_list.insert(0, "Original")
    title_list.insert(1, "Threshold")
    title_list.insert(2, "kernel")
    
    image_list = [image for image in __GLOBAL_BUFFER.values()]
    image_list.insert(0, image)
    image_list.insert(1, image_threshold)
    image_list.insert(2, kernel)

    show_image(
        image_list=image_list, 
        open_order=3 if len(image_list) > 3 else len(image_list), 
        title=title_list
    )
